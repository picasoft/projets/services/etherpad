#!/usr/bin/env bash
set -e

# Change default hard-coded pad title
sed -i 's|Untitled Pad|Nouveau pad|g' \
	/opt/etherpad-lite/src/node_modules/ep_set_title_on_pad/templates/title.ejs \
	/opt/etherpad-lite/src/node_modules/ep_set_title_on_pad/static/tests/frontend/specs/title.js


# Create random API key in a shared volume
# See https://github.com/ether/etherpad-lite/issues/6350#issuecomment-2330823537
KEYPATH=/api-key/api-key.txt
tr -dc A-Za-z0-9 </dev/urandom | head -c 60 > ${KEYPATH}
cp ${KEYPATH} /opt/etherpad-lite/APIKEY.txt

# Merge custom settings with upstream settings
# We move here to have access to the jsonminify module used by etherpad to remove comments in JSON
cd /opt/etherpad-lite/src
node <<EOF
try {
	const fs = require('fs');
	const JSON5 = require('json5')

	const readJSON = filename => JSON5.parse(fs.readFileSync(filename, 'utf8'));

	const upstream = readJSON('/opt/etherpad-lite/settings.json');
	const plugins = readJSON('/plugin-settings.json');

	const merged = { ...upstream, ...plugins };

	fs.writeFileSync('/opt/etherpad-lite/settings.json', JSON.stringify(merged));
} catch (e) {
	console.log("Error while merging settings in Docker entrypoint.sh");
	console.log(e);
	process.exit(1);
}

EOF

# Prevent secret duplication in ertherpad-db.secrets and etherpad-app.secrets
# Seems more robust : https://stackoverflow.com/questions/22869637/setting-variables-with-executing-a-command-with-exec
exec env DB_PASS=${POSTGRES_PASSWORD} DEFAULT_PAD_TEXT="$(echo "$DEFAULT_PAD_TEXT" | sed "s/DELETE_AFTER_DELAY_HUMAN/$DELETE_AFTER_DELAY_HUMAN/")" DELETE_AFTER_DELAY_SENTENCE="$(echo "$DELETE_AFTER_DELAY_SENTENCE" | sed "s/DELETE_AFTER_DELAY_HUMAN/$DELETE_AFTER_DELAY_HUMAN/")" "$@"
