function go()
{
	instanceURL = 'https://week.pad.picasoft.net/p/'
	padName = document.getElementById('padInput1').value

	if (padName === "")
	{
		//this should not happen
		padName = randomPadName()
	}

	if (document.getElementById("radio_normal").checked)
	{
		instanceURL = 'https://pad.picasoft.net/p/'
	}

	document.location = instanceURL + padName
}

function randomize()
{
	instanceURL = 'https://week.pad.picasoft.net/p/'
	if (document.getElementById("radio_normal").checked)
	{
		instanceURL = 'https://pad.picasoft.net/p/'
	}
	document.location = instanceURL + randomPadName()
}

function onModification(padInput)
{
	if (document.getElementById(padInput).value !== "")
	{
		document.getElementById('goButton').classList.add("btn-primary")
		document.getElementById('goButton').classList.remove("btn-secondary")
		document.getElementById('goButton').disabled = false
	}
	else
	{
		document.getElementById('goButton').classList.remove("btn-primary")
		document.getElementById('goButton').classList.add("btn-secondary")
		document.getElementById('goButton').disabled = true
	}
}


/*
function genRandomName(complexity)
{
	var lenAd = DAdS.length

	if (complexity === 1)
	{
		return getRandomAnimal() + getRandomAdjectif()
	}
	else
	{
		return getRandomAnimal() + getRandomAdjectif() + "Et" + genRandomName(complexity-1)
	}
}

function getRandomAnimal()
{
	var len = DAnS.length
	return DAnS[Math.floor(Math.random()* (len-1))]
}

function getRandomAdjectif()
{
	var len = DAdS.length
	return DAdS[Math.floor(Math.random()* (len-1))]
}

*/

function randomPadName()
{
	var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
	var string_length = 10;
	var randomstring = '';

	for (var i = 0; i < string_length; i++)
	{
		var rnum = Math.floor(Math.random() * chars.length);
		randomstring += chars.substring(rnum, rnum + 1);
	}
	return randomstring;
}


onModification('padInput1')
