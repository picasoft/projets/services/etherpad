# Picapad

Ce dossier contient une image d'Etherpad Lite maintenue par l'association.

Pour une documentation générale (administration, maintenance), voir [le wiki](https://wiki.picasoft.net/doku.php?id=technique:adminserv:etherpad:start).

Ce repository contient un fichier compose capable de build l'image et lancer une des deux instances.
C'est le `.env` qui va faire la différence entre l'instance "yearly" et l'instance "weekly".

## Lancement et tests

Au premier lancement, il faut :
- initialiser le fichier `.env` en copiant le `.env.example` et en le personnalisant selon les besoins.
- `docker compose up -d && docker compose exec -u root app chown etherpad /api-key/ && docker compose restart app` pour donner les droits à etherpad de créer le fichier contenant la clé d'API nécessaire au fonctionnement du conteneur `delete-pad-after-delay`.

## Mise à jour de l'image

Pour mettre à jour la version d'Etherpad, il faut simplement modifier le numéro de version à la ligne `image:` et à la ligne `context:`.

On vérifiera qu'il n'y a pas de nouveaux paramètres importants dans le fichier `settings.json`, sur l'[historique GitHub](https://github.com/ether/etherpad-lite/commits/develop/settings.json.docker).

Pour PostgreSQL, voir [ce tuto](https://thomasbandt.com/postgres-docker-major-version-upgrade) à titre d'exemple.

Avant de mettre à jour le service en production, pensez à [prévenir tous les utilisateurs connectés](https://wiki.picasoft.net/doku.php?id=technique:adminserv:etherpad:admin_gui#message_a_tout_e_s).

## Configuration

Etherpad se configure au lancement du conteneur avec des variables d'environnement. Elles sont présentes à deux endroits :

- Les fichiers dans les sous-dossiers `secrets` contiennent les variables nécessaires pour stocker les mots de passe de la base de données et du user admin d'etherpad.
- Le reste des variables d'environnement non-confidentielles est affecté directement dans les fichiers Compose, via la directive `environment`.

Toutes les paramètres sont configurables via l'environnement. Des explications sont [disponibles ici](https://github.com/ether/etherpad-lite/blob/develop/settings.json.docker). Ce fichier contient uniquement les valeurs par défaut. La configuration doit avoir lieu dans les fichiers Compose.

Pour le bon fonctionnement des métriques, il faut aussi créer un fichier `.env` (dans le même dossier que le Docker Compose) qui devra contenir une variable `METRICS_AUTH`. Cette variable correspond à la chaîne d'identification htpasswd utilisée pour authentifier sur l'endpoint des métriques, par exemple `METRICS_AUTH="etherpad:$apr1$bXnknJ0S$GsC.ozNJc/dAkh9uH7Qlg."`

## Ajout d'un plugin

Etherpad maintient une [liste officielle des plugins](https://static.etherpad.org/plugins.html).

Pour installer un plugin, on évitera de passer par l'interface administrateur et on préfèrera modifier la liste des arguments de build dans le docker-compose.yml.

Il suffit pour ce faire d'ajouter le nom du package npm sur la ligne `ETHERPAD_PLUGINS`, en respectant l'ordre alphabétique pour la facilité de lecture.

## Page d'accueil

La page d'accueil est présente dans le dossier [landing-page](./landing-page).

La même page d'accueil est utilisée pour les deux instances. Pour la modifier effectivement, il faudra pousser les modifications et reconstruire l'image.

Lors de l'ajout de nouvelles images, on conseille de les minifier avec `optipng` et `jpegoptim`.
