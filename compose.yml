volumes:
  db:
    name: etherpad-${INSTANCE_PREFIX}-db
  deleted-pads:
    name: etherpad-${INSTANCE_PREFIX}-deleted-pads
  api-key:
    name: etherpad-${INSTANCE_PREFIX}-api-key

networks:
  db:
    name: etherpad-${INSTANCE_PREFIX}-db
  dpad:
    name: etherpad-${INSTANCE_PREFIX}-deleted-pads
  proxy:
    external: true

services:
  db:
    image: postgres:17-alpine
    container_name: etherpad-${INSTANCE_PREFIX}-db
    volumes:
      - db:/var/lib/postgresql/data
    env_file: ./secrets/etherpad-db.secrets
    environment:
      POSTGRES_USER: etherpad
      POSTGRES_DB: etherpad
    networks:
      - db
    restart: unless-stopped

  app:
    image: registry.picasoft.net/pica-etherpad:2.2.7
    build:
      context: https://github.com/ether/etherpad-lite.git
      args:
        # Cf https://github.com/ether/etherpad-lite/issues/6813
        BUILD_ENV: copy
        INSTALL_ABIWORD: 1
        ETHERPAD_PLUGINS: >-
          ep_adminpads2 ep_align ep_author_hover ep_back_button ep_chatdate
          ep_comments_page ep_delete_after_delay ep_delete_empty_pads ep_font_color
          ep_font_family ep_headings2 ep_pad_activity_nofication_in_title
          ep_prometheus_exporter ep_prompt_for_name ep_set_title_on_pad
          ep_subscript_and_superscript ep_spellcheck ep_message_all ep_font_size
          json5

    container_name: etherpad-${INSTANCE_PREFIX}-app
    env_file:
      - ./secrets/etherpad-app.secrets
      - ./secrets/etherpad-db.secrets
      - ./.env
    entrypoint: /entrypoint.sh
    volumes:
      - api-key:/api-key
      - ./plugin-settings.json:/plugin-settings.json:ro
      - ./entrypoint.sh:/entrypoint.sh:ro
      - ./landing-page/index.html:/opt/etherpad-lite/src/templates/index.html:ro
      - ./landing-page/static/custom:/opt/etherpad-lite/src/static/custom:ro
    command: ["pnpm", "run", "prod"]
    labels:
      - "traefik.http.routers.etherpad-${INSTANCE_PREFIX}-app.entrypoints=websecure"
      - "traefik.http.routers.etherpad-${INSTANCE_PREFIX}-app.rule=Host(`${INSTANCE_URL}`)"
      - "traefik.http.services.etherpad-${INSTANCE_PREFIX}-app.loadbalancer.server.port=9001"
      - "traefik.http.routers.etherpad-${INSTANCE_PREFIX}-metrics.rule=Host(`${INSTANCE_URL}`) && PathPrefix(`/metrics`)"
      - "traefik.http.routers.etherpad-${INSTANCE_PREFIX}-metrics.middlewares=etherpad-${INSTANCE_PREFIX}-metrics-auth@docker"
      - "traefik.http.middlewares.etherpad-${INSTANCE_PREFIX}-metrics-auth.basicauth.users=${METRICS_AUTH}"
      - "traefik.enable=true"
    environment:
      DB_HOST: etherpad-${INSTANCE_PREFIX}-db
      DB_PORT: 5432
      DB_USER: etherpad
      DB_NAME: etherpad
      DB_TYPE: postgres
      PAD_OPTIONS_USER_NAME: anonyme
      PAD_OPTIONS_LANG: fr
      FAVICON: https://picasoft.net/res/picasoft-logo-etherpad.png
      TITLE: Picapad
      ABIWORD: /usr/bin/abiword
      # Cf https://github.com/ether/etherpad-lite/issues/6350
      AUTHENTICATION_METHOD: apikey
      SUPPRESS_ERRORS_IN_PAD_TEXT: "true"
      # See https://github.com/ether/etherpad-lite/issues/5544 : avoid errors on large pastes
      SOCKETIO_MAX_HTTP_BUFFER_SIZE: 200000
      ALLOW_UNKNOWN_FILE_ENDS: "false"
      TRUST_PROXY: "true"
      AUTOMATIC_RECONNECTION_TIMEOUT: 10
      FOCUS_LINE_PERCENTAGE_ABOVE: 0.5
      # FOCUS_LINE_PERCENTAGE_BELOW: 0.5
      FOCUS_LINE_DURATION: 200
      DELETE_AFTER_DELAY_SENTENCE: Le contenu de ce pad a été supprimé pour cause d'inactivité pendant DELETE_AFTER_DELAY_HUMAN.
      DEFAULT_PAD_TEXT: |
        Bienvenue sur Picapad, une instance d'Etherpad, un éditeur de texte collaboratif libre.
        -----------------------------------------------------------
        ATTENTION : tous les pads de cette instance seront supprimés après DELETE_AFTER_DELAY_HUMAN d'inactivité ! Nous conservons un backup des pads supprimés en cas d'erreur : n'hésitez pas à nous envoyer un message pour récupérer le contenu d'un pad que vous auriez perdu.

        Le texte que vous saisissez est automatiquement synchronisé avec toutes les personnes naviguant sur ce pad.

        Prenez des notes et rédigez des documents librement !

        → Pour bien commencer :
        • Renseignez votre nom ou pseudo, en cliquant sur l’icône « utilisateur » en haut à droite.
        • Choisissez votre couleur d'écriture au même endroit.
        • Les contributions de chacun se synchronisent « en temps réel » sous leur propre couleur.
        • Un chat vous permet de discuter avec les autres personnes présentes sur le pad.

        → Fonctionnalités :
        • Couleur du texte, tableau, choix et taille de police, alignement, pleine page...
        • Sauvegarde automatique du pad.
        • Historique complet du pad (bouton en forme d'horloge)
        • Sauvegarde de versions clés (bouton en forme d'étoile).
        • Commentaires avec suggestion de remplacement (bouton en forme de bulle).
        • Les réglages vous permettent de désactiver les couleurs, de changer la langue, d'activer les sauts de pages...

        → Partage :
        • Import et export dans divers formats (bouton avec les flèches).
        • Partage en lecture seule, pour éviter les modifications non voulues (bouton </>).

        Pensez à garder l'URL de votre pad pour le retrouver.
        Attention, celui-ci est public, c'est-à-dire que toute personne qui en possède l'URL pourra y accéder : n'y stockez pas d'informations confidentielles !
        -----------------------------------------------------------
        Une question ? Un problème ? Envoyez-nous un mail à picasoft@assos.utc.fr !

    networks:
      - proxy
      - db
      - dpad
    depends_on:
      - db
    restart: unless-stopped

  delete-pad-after-delay:
    image: registry.picasoft.net/pica-etherpad-delete-after-delay:picasoft-docker.1.0.2
    container_name: etherpad-${INSTANCE_PREFIX}-delete-pad
    build:
      context: https://gitlab.utc.fr/picasoft/projets/delete-pad-after-delay.git#picasoft-docker.1.0.2
    volumes:
      - api-key:/api-key:ro
      - deleted-pads:/opt/etherpad-lite/deleted_pads
    networks:
      - dpad
    environment:
      # Must match app container name and port
      URL: "http://etherpad-${INSTANCE_PREFIX}-app:9001"
      # In seconds. MUST be COHERENT with settings.json
      DEL: ${DELETE_AFTER_DELAY}
      # Same mount point than deleted-pads
      DIR: "/opt/etherpad-lite/deleted_pads"
      # Same mount point than api-key + api-key.txt
      APIKEY_PATH: "/api-key/api-key.txt"
    depends_on:
      - app
    restart: unless-stopped
